package facci.pm.alava.delgado.escuelasdemanta;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import java.text.Normalizer;

public class HelperDB extends SQLiteOpenHelper {


    private static int version = 1;
    private static String nombre = "EscuelasManta" ;
    private static CursorFactory cursorFactory = null;

    private static String id = "id";
    private static String Numero = "Numero_Registro";
    private static String Nombre = "Nombre_Rectora";
    private static String Especialidad = "Especialidad";
    private static String Seccion = "Seccion";
    private static String Formacion = "Formacion";
    private static String NombreTabla= "Escuelas";

    public HelperDB(Context context) {
        super(context, nombre, cursorFactory, version);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + NombreTabla + "(" +
                 id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                Numero + " VARCHAR," +
                Nombre+" VARCHAR," +
                Especialidad + " VARCHAR," +
                Seccion+" VARCHAR, " +
                Formacion+" VARCHAR)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String sql = "DROP TABLE IF EXISTS " + NombreTabla;
        db.execSQL(sql);
        onCreate(db);
    }

    public void Insertar(String NumeroR, String NombreR, String EspecialidadR, String SeccionR, String FormacionR){

        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Numero, NumeroR);
        contentValues.put(Nombre, NombreR);
        contentValues.put(Especialidad, EspecialidadR);
        contentValues.put(Seccion, SeccionR);
        contentValues.put(Formacion, FormacionR);
        database.insert(NombreTabla,null,contentValues);
    }

    public void Eliminar(String NumeroR){
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(NombreTabla, Numero +"= " + NumeroR, null);
    }

    public void EliminarTodo(){
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(NombreTabla, null, null);
    }

    public void Actualizar(String NumeroR, String NombreR, String EspecialidadR, String SeccionR, String FormacionR){

        SQLiteDatabase database = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(Numero, NumeroR);
        contentValues.put(Nombre, NombreR);
        contentValues.put(Especialidad, EspecialidadR);
        contentValues.put(Seccion, SeccionR);
        contentValues.put(Formacion, FormacionR);
        database.update(NombreTabla, contentValues, Numero + "= " + NumeroR, null);
    }

    public String Consultar(String NumeroR){
        SQLiteDatabase database = this.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String result = "";
        String[] sqlSelect = {Numero, Nombre, Especialidad, Seccion, Formacion};
        String sqlTable = NombreTabla;
        String[] args = new String[] {NumeroR};
        qb.setTables(sqlTable);
        Cursor c = qb.query(database, sqlSelect, Numero+"=?", args, null, null, null);
        if (c.moveToFirst()) {
            do {
                result += Numero +" " + c.getString(c.getColumnIndex(Numero)) + "\n" +
                        Nombre + " " + c.getString(c.getColumnIndex(Nombre)) + "\n" +
                        Especialidad +" " + c.getString(c.getColumnIndex(Especialidad)) + "\n" +
                        Seccion + " " + c.getString(c.getColumnIndex(Seccion)) + "\n" +
                        Formacion+" " + c.getString(c.getColumnIndex(Formacion)) + "\n\n\n";
            } while (c.moveToNext());
            c.close();
            database.close();
        }
        return result;

    }

    public String ConsultarTodos(){
        SQLiteDatabase database = this.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String result = "";
        String[] sqlSelect = {Numero, Nombre, Especialidad, Seccion, Formacion};
        String sqlTable = NombreTabla;
        qb.setTables(sqlTable);
        Cursor c = qb.query(database, sqlSelect, null, null, null, null, null);
        if (c.moveToFirst()) {
            do {
                result += Numero +" " + c.getString(c.getColumnIndex(Numero)) + "\n" +
                        Nombre + " " + c.getString(c.getColumnIndex(Nombre)) + "\n" +
                        Especialidad +" " + c.getString(c.getColumnIndex(Especialidad)) + "\n" +
                        Seccion + " " + c.getString(c.getColumnIndex(Seccion)) + "\n" +
                        Formacion+" " + c.getString(c.getColumnIndex(Formacion)) + "\n\n\n";
            } while (c.moveToNext());
            c.close();
            database.close();
        }
        return result;

    }

}
