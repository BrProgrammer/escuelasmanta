package facci.pm.alava.delgado.escuelasdemanta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.ContentValues;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    private EditText NumeroR, NombresR, Especialidad, Seccion, Formacion;
    private Button Insertar, Consultar, ConsultarT, actualizar, Eliminar, EliminarT;
    private TextView Datos;
    private HelperDB helperDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NumeroR = (EditText)findViewById(R.id.Numero);
        NombresR = (EditText)findViewById(R.id.Nombre);
        Especialidad = (EditText)findViewById(R.id.Especialidad);
        Seccion = (EditText)findViewById(R.id.Seccion);
        Formacion = (EditText)findViewById(R.id.Formacion);

        Insertar = (Button)findViewById(R.id.Insertar);
        Consultar = (Button)findViewById(R.id.Consultar);
        ConsultarT = (Button)findViewById(R.id.ConsultarT);
        actualizar = (Button)findViewById(R.id.Actualizar);
        Eliminar = (Button)findViewById(R.id.Eliminar);
        EliminarT = (Button)findViewById(R.id.EliminarT);

        Datos = (TextView)findViewById(R.id.Datos);

        Insertar.setOnClickListener(this);
        Consultar.setOnClickListener(this);
        ConsultarT.setOnClickListener(this);
        actualizar.setOnClickListener(this);
        Eliminar.setOnClickListener(this);
        EliminarT.setOnClickListener(this);

        helperDB = new HelperDB(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.EliminarT:

                helperDB.EliminarTodo();
                Toast.makeText(this, "ELIMINADOS", Toast.LENGTH_SHORT).show();
                Datos.setText("");
                break;

            case R.id.Eliminar:

                if (NumeroR.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE EL NUMERO DE REGISTRO QUE DESEA ELIMINAR", Toast.LENGTH_SHORT).show();
                    NumeroR.requestFocus();
                }else {
                    helperDB.Eliminar(NumeroR.getText().toString());
                    Toast.makeText(this, "ELIMINADO", Toast.LENGTH_SHORT).show();
                    NumeroR.setText("");
                    Datos.setText("");
                }

                break;

            case R.id.ConsultarT:
                Datos.setText(helperDB.ConsultarTodos());
                break;

            case R.id.Consultar:

                if (NumeroR.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE EL NUMERO DE REGISTRO QUE DESEA CONSULTAr", Toast.LENGTH_SHORT).show();
                    NumeroR.requestFocus();
                }else {
                    Datos.setText(helperDB.Consultar(NumeroR.getText().toString()));
                    NumeroR.setText("");
                }
                break;

            case R.id.Insertar:

                if (NumeroR.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE UN DATO", Toast.LENGTH_SHORT).show();
                    NumeroR.requestFocus();
                }else if(NombresR.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE UN DATO", Toast.LENGTH_SHORT).show();
                    NombresR.requestFocus();
                }else if(Especialidad.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE UN DATO", Toast.LENGTH_SHORT).show();
                    Especialidad.requestFocus();
                }else if (Seccion.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE UN DATO", Toast.LENGTH_SHORT).show();
                    Seccion.requestFocus();
                }else if(Formacion.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE UN DATO", Toast.LENGTH_SHORT).show();
                    Formacion.requestFocus();
                }else {
                    helperDB.Insertar(NumeroR.getText().toString(), NombresR.getText().toString(),
                            Especialidad.getText().toString(), Seccion.getText().toString(),
                            Formacion.getText().toString());
                    Toast.makeText(this, "INSERTADO", Toast.LENGTH_SHORT).show();
                    NombresR.setText("");
                    NumeroR.setText("");
                    Especialidad.setText("");
                    Seccion.setText("");
                    Formacion.setText("");
                    Datos.setText("");
                }

                break;

            case R.id.Actualizar:

                if (NumeroR.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE UN DATO", Toast.LENGTH_SHORT).show();
                    NumeroR.requestFocus();
                }else if(NombresR.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE UN DATO", Toast.LENGTH_SHORT).show();
                    NombresR.requestFocus();
                }else if(Especialidad.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE UN DATO", Toast.LENGTH_SHORT).show();
                    Especialidad.requestFocus();
                }else if (Seccion.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE UN DATO", Toast.LENGTH_SHORT).show();
                    Seccion.requestFocus();
                }else if(Formacion.getText().toString().isEmpty()){
                    Toast.makeText(this, "INSERTE UN DATO", Toast.LENGTH_SHORT).show();
                    Formacion.requestFocus();
                }else {
                    helperDB.Actualizar(NumeroR.getText().toString(), NombresR.getText().toString(),
                            Especialidad.getText().toString(), Seccion.getText().toString(),
                            Formacion.getText().toString());
                    Toast.makeText(this, "ACTUALIZADO", Toast.LENGTH_SHORT).show();
                    NombresR.setText("");
                    NumeroR.setText("");
                    Especialidad.setText("");
                    Seccion.setText("");
                    Formacion.setText("");
                    Datos.setText("");
                }

                break;
        }

    }
}
